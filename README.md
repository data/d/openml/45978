# OpenML dataset: ILINet

https://www.openml.org/d/45978

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Outpatient Illness Surveillance - Information on patient visits to health care providers for influenza-like illness is collected through the U.S. Outpatient Influenza-like Illness Surveillance Network (ILINet). This collaborative effort between CDC, state and local health departments, and health care providers started during the 1997-98 influenza season when approximately 250 providers were enrolled. Enrollment in the system has increased over time and there were >3,000 providers enrolled during the 2010-11 season.

The number and percent of patients presenting with ILI each week will vary by region and season due to many factors, including having different provider type mixes (children present with higher rates of ILI than adults, and therefore regions with a higher percentage of pediatric practices will have higher numbers of cases). Therefore it is not appropriate to compare the magnitude of the percent of visits due to ILI between regions and seasons.

Baseline levels are calculated both nationally and for each region. Percentages at or above the baseline level are considered to be elevated.

For more information on ILI surveillance and baselines please visit:http://www.cdc.gov/flu/weekly/overview.htm#Outpatient

This data is the extraction of "National" data from seasons 1997-98 to 2023-24.

We have applied some minor preprocessing:

1 - Dropped columns 'REGION' and 'REGION TYPE', as they have only the value 'X'.

2 - Dropped rows with 'YEAR' <= 2002 and 'YEAR' >= 2024.

Before the year 2002, there is a seasonal gap every year between the weeks [21, 39]. This does not happen after 2002. Effectively,
this drop 274 rows, or ~20% of the original amount. We could imagine that a model will automatically account for this, but
we prefered to work with a clean dataset as it is already common for this dataset in other works. Besides, the data is not yet
completed for 2024.

2 - Replaced values 'X' by 0, and casted columns 'AGE 25-49', 'AGE 50-64', and 'AGE 25-64' to int.

3 - Summed columns 'AGE 25-49', 'AGE 50-64', and 'AGE 25-64' to replace the column 'AGE 25-64'.

4 - Dropped columns AGE 25-49', 'AGE 50-64'.

It seems that the values 'X' in the 'AGE X' columns are due to a change on how the age of the patients were accounted for
before and after the year-week 2009-40. With our preprocessing, we correctly find the 'ILITOTAL' if we sum all the 'AGE X' columns.

5 - Created date column 'date' from columns 'YEAR' and 'WEEK', considering the end of week on Saturday. We should be able to parse it using pandas
```to_datetime``` function.

6 - Dropped columns 'YEAR' and 'MONTH'.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45978) of an [OpenML dataset](https://www.openml.org/d/45978). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45978/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45978/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45978/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

